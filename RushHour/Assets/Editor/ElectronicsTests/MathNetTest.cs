﻿using System;
using UnityEngine;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using NUnit.Framework;

public class MathNetTest
{
		
	[Test]
	public void IdentityTest () {
		// write simple matrices
		Matrix<double> E = DenseMatrix.OfArray (new double[,] {
			{ 1, 0, 0 },
			{ 0, 1, 0 },
			{ 0, 0, 1 }
		});
		Assert.AreEqual (E, DenseMatrix.CreateIdentity (3));
	}

	[Test]
	public void EmptyMatrixTest () {
		Matrix<double> A = null;
		Assert.AreEqual (true, MatrixManipulations.isMatrixEmpty (A));
	}

	[Test]
	public void NonEmptyMatrixTest () {
		Matrix<double> A = DenseMatrix.CreateIdentity(4);
		Assert.AreEqual (false, MatrixManipulations.isMatrixEmpty (A));
	}

	[Test]
	public void SolveEquationTest () {
		// solve a linear equation
		Matrix<double> A = Matrix<double>.Build.DenseOfArray(new double[,] {
			{ 2,  3, -1},
			{ 1,  3,  1},
			{-2, -2,  4}
		});
		var b = Vector<double>.Build.Dense (new double[] { 1, 2, 4 });
		var x = A.Solve (b);
		var y = Vector<double>.Build.Dense (new double[] { 3, -1, 2 });
		Assert.AreEqual (y, x);
	}

	[Test]
	public void MatrixExtendedTest () {
		Matrix<double> A = DenseMatrix.OfArray (new double[,] {
			{ 1, 1, 1, 1 },
			{ 1, 2, 3, 4 },
			{ 4, 3, 2, 1 }
		});

		Matrix<double> expected_A = DenseMatrix.OfArray (new double[,] {
			{ 1, 1, 0, 1, 1 },
			{ 1, 2, 0, 3, 4 },
			{ 0, 0, 0, 0, 0 },
			{ 4, 3, 0, 2, 1 }
		});

		int i = 2;
		Matrix<double> returned_A = MatrixManipulations.extendMatrix (A, i);
		Assert.AreEqual (expected_A, returned_A);
		Assert.AreNotEqual (expected_A, A);
	}

	[Test]
	public void EmptyMatrixExtendedTest () {
		Matrix<double> A = null;
		int i = 0;
		Matrix<double> returned_A = MatrixManipulations.extendMatrix(A, i);
		Assert.AreEqual(DenseMatrix.Build.Dense(1,1), returned_A);
	}

	[Test]
	public void MatrixReducedTest () {
		Matrix<double> A = DenseMatrix.OfArray (new double[,] {
			{ 1, 1, 0, 1, 1 },
			{ 1, 2, 0, 3, 4 },
			{ 0, 0, 0, 0, 0 },
			{ 4, 3, 0, 2, 1 }
		});

		Matrix<double> expected_A = DenseMatrix.OfArray (new double[,] {
			{ 1, 1, 1, 1 },
			{ 1, 2, 3, 4 },
			{ 4, 3, 2, 1 }
		});

		int i = 2;
		Matrix<double> returned_A = MatrixManipulations.reduceMatrix (A, i);
		Assert.AreEqual (expected_A, returned_A);
		Assert.AreNotEqual (expected_A, A);
	}

	[Test]
	public void ValueAddedTest () {
		Matrix<double> A = DenseMatrix.OfArray (new double[,] {
			{ 1, 1, 1, 1 },
			{ 1, 2, 3, 4 },
			{ 4, 3, 2, 1 }
		});

		Matrix<double> expected_A = DenseMatrix.OfArray (new double[,] {
			{ 1, 1,   1, 1 },
			{ 1, 2,   3, 4 },
			{ 4, 3, 2.5, 1 }
		});

		int i = 2;
		double value = 0.5;
		Matrix<double> returned_A = MatrixManipulations.addValueToMatrix (A, i, value);
		Assert.AreEqual (expected_A, returned_A);
		Assert.AreEqual (expected_A, A);
	}

	[Test]
	public void ExtendVectorTest () {
		Vector<double> v = DenseVector.OfArray (new double[] { 1, 2, 4 });
		Vector<double> expected_v = DenseVector.OfArray (new double[] { 1, 2, 0, 4 });

		int i = 2;
		Vector<double> returned_v = MatrixManipulations.extendVector (v, i);
		Assert.AreEqual (expected_v, returned_v);
		Assert.AreNotEqual (expected_v, v);
	}

	[Test]
	public void EmptyVectorExtendedTest () {
		Vector<double> b = null;
		int i = 0;
		Vector<double> returned_v = MatrixManipulations.extendVector(b, i);
		Assert.AreEqual(DenseVector.Build.Dense(1), returned_v);
	}
}
﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using MathNet.Numerics;

public class NodeEquivalenceTest {

	[Test]
	public void positionsNotEquivalent () {
		Position position1 = new Position (2, 3);
		Position position2 = new Position (2, 4);

		Assert.IsFalse (position1.Equals (position2));
	}

	[Test]
	public void positionsEquivalent () {
		Position position1 = new Position (2, 3);
		Position position2 = new Position (2, 3);

		Assert.IsTrue (position1.Equals (position2));
	}

	[Test]
	public void nodesNotEquivalent () {
		Node node1 = new Node (2, 3, 2, 4);
		Node node2 = new Node (2, 3, 3, 3);

		Assert.IsFalse (node1.Equals (node2));
	}

	[Test]
	public void nodesEquivalent () {
		Node node1 = new Node (2, 3, 2, 4);
		Node node2 = new Node (2, 3, 2, 4);

		Assert.IsTrue (node1.Equals (node2));
	}

	[Test]
	public void nodeAlreadyExistsTest () {
		Node node = new Node (2, 3, 2, 4);

		List<Node> nodeList = new List<Node> ();
		nodeList.Add (node);
		ElectronicGrid electronicGrid = new ElectronicGrid ();
		electronicGrid.equivalentNodes.Add (nodeList);

		Assert.IsTrue(node.nodeAlreadyExists (electronicGrid.equivalentNodes));

		electronicGrid.equivalentNodes.Remove (nodeList);

		Assert.IsFalse(node.nodeAlreadyExists (electronicGrid.equivalentNodes));
	}

	[Test]
	public void nodeDoesNotExistsTest () {
		Node node = new Node (2, 3, 2, 4);

		ElectronicGrid electronicGrid = new ElectronicGrid ();

		Assert.IsFalse(node.nodeAlreadyExists (electronicGrid.equivalentNodes));
	}

	[Test]
	public void getNodeIndexOfNonExistingNode () {
		ElectronicGrid electronicGrid = new ElectronicGrid ();

		Node node = new Node (2, 3, 2, 5);
		int nodeIndex = node.findNode (electronicGrid.equivalentNodes);
		int nodeIndexExpected = -1;

		Assert.AreEqual (nodeIndexExpected, nodeIndex);
	}

	[Test]
	public void getNodeIndexOfExistingNode1 () {
		Node node = new Node (2, 3, 2, 4);
		int nodeIndex = node.findNode (createFakeNodeList ());
		int nodeIndexExpected = 0;

		Assert.AreEqual (nodeIndexExpected, nodeIndex);
	}

	[Test]
	public void getNodeIndexOfExistingNode2 () {
		Node node = new Node (2, 2, 2, 3);
		int nodeIndex = node.findNode (createFakeNodeList ());
		int nodeIndexExpected = 1;

		Assert.AreEqual (nodeIndexExpected, nodeIndex);
	}

	[Test]
	public void getNodeIndexOfExistingNode3 () {
		Node node = new Node (2, 3, 3, 3);
		int nodeIndex = node.findNode (createFakeNodeList ());
		int nodeIndexExpected = 1;

		Assert.AreEqual (nodeIndexExpected, nodeIndex);
	}

	[Test]
	public void getNodeIndexOfExistingNode4 () {
		Node node = new Node (2, 2, 3, 2);
		int nodeIndex = node.findNode (createFakeNodeList ());
		int nodeIndexExpected = 1;

		Assert.AreEqual (nodeIndexExpected, nodeIndex);
	}

	[Test]
	public void getNodeIndexOfNonExistingNode1 () {
		Node node = new Node (2, 2, 3, 2);
		int nodeIndex = node.findNode (createFakeNodeList ());
		int nodeIndexExpected = 1;

		Assert.AreEqual (nodeIndexExpected, nodeIndex);
	}

	public List<List<Node>> createFakeNodeList() {
		Node node1 = new Node (2, 3, 2, 4);
		Node node2 = new Node (2, 2, 2, 3);
		Node node3 = new Node (2, 3, 3, 3);
		Node node4 = new Node (2, 2, 3, 2);

		List<Node> nodeList1 = new List<Node> ();
		nodeList1.Add (node1);
		List<Node> nodeList2 = new List<Node> ();
		nodeList2.Add (node2);
		nodeList2.Add (node3);
		nodeList2.Add (node4);

		List< List<Node>> nodesList = new List< List<Node>> ();
		nodesList.Add (nodeList1);
		nodesList.Add (nodeList2);

		return nodesList;
	}
		
}

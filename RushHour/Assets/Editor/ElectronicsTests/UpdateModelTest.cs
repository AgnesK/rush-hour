﻿using System;
using NUnit.Framework;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using UnityEngine;

public class UpdateModelTest {

	ElectronicGrid electronicGridTest;

	[SetUp]
	public void TestSetUp () {
		electronicGridTest = new ElectronicGrid ();
	}

	[Test]
	public void InitialModelTest () {
		Matrix<double> newMNAMatrix = DenseMatrix.Build.DenseOfArray(new double[,] {
			{ 0, 1},
			{ 1, 0}
		});

		Vector<double> newSourceVector = DenseVector.OfArray (new double[] { 0, 5 });

		Vector<double> newSolutionVector = DenseVector.OfArray (new double[] { 5, 0 });

		Assert.AreEqual (newMNAMatrix, electronicGridTest.MNAMatrix);
		Assert.AreEqual (newSourceVector, electronicGridTest.sourceVector);
		Assert.AreEqual (newSolutionVector, electronicGridTest.solutionVector);
	}

	[Test]
	public void AddWireWithNoExistingNodesTest () {
		//Add Wire
		Node nodeA = new Node (1, 0, 1, 1);
		Node nodeB = new Node (1, 1, 1, 2);
		Position componentPosition = new Position (0, 1);
		Wire wire = new Wire(nodeA, nodeB, componentPosition); 
		electronicGridTest.AddComponent(wire);

		//Check if there is a list in the equivalentNodes List that contains the two nodes of the new wire
		int nodeAIndex = nodeA.findNode (electronicGridTest.equivalentNodes);
		int nodeBIndex = nodeB.findNode (electronicGridTest.equivalentNodes);
		Assert.AreEqual(nodeAIndex, nodeBIndex);
		Assert.AreNotEqual (-1, nodeAIndex);

		//Check if model was updated correctly

		Matrix<double> newMNAMatrix = DenseMatrix.Build.DenseOfArray(new double[,] {
			{  0, 0, 1},
			{  0, 0, 0},
			{  1, 0, 0}
		});

		Vector<double> newSourceVector = DenseVector.OfArray (new double[] { 0, 0, 5 });

		Vector<double> newSolutionVector = DenseVector.OfArray (new double[] { 5, 0, 0 });

		Assert.AreEqual (newMNAMatrix, electronicGridTest.MNAMatrix);
		Assert.AreEqual (newSourceVector, electronicGridTest.sourceVector);
	}

	[Test]
	public void AddWireWithOneExistingNodesTest () {
		//Add Wire
		Node nodeA = new Node (0, 0, 0, 1);
		Node nodeB = new Node (0, 0, 1, 0);
		Position componentPosition = new Position (0, 0);
		Wire wire = new Wire(nodeA, nodeB, componentPosition); 
		electronicGridTest.AddComponent(wire);

		//Check if there is a list in the equivalentNodes List that contains the two nodes of the new wire
		int nodeAIndex = nodeA.findNode (electronicGridTest.equivalentNodes);
		int nodeBIndex = nodeB.findNode (electronicGridTest.equivalentNodes);
		Assert.AreEqual(nodeAIndex, nodeBIndex);
		Assert.AreNotEqual (-1, nodeAIndex);

		//Check if model was not-updated correctly

		Matrix<double> newMNAMatrix = DenseMatrix.Build.DenseOfArray(new double[,] {
			{  0, 1},
			{  1, 0}
		});

		Vector<double> newSourceVector = DenseVector.OfArray (new double[] { 0, 5 });

		Vector<double> newSolutionVector = DenseVector.OfArray (new double[] { 5, 0 });

		Assert.AreEqual (newMNAMatrix, electronicGridTest.MNAMatrix);
		Assert.AreEqual (newSourceVector, electronicGridTest.sourceVector);
		Assert.AreEqual (newSolutionVector, electronicGridTest.solutionVector);
	}

	[Test]
	public void AddWireWithTwoExistingNodesTest () {
		Assert.Fail();
	}

	[Test]
	public void AddResistorToExistingNodesTest () {
		Node nodeA = new Node (0, 0, 0, 1);
		Node nodeB = new Node (0, 1, 0, 2);
		Position componentPosition = new Position (0, 1);
		double resistance = 2;
		Resistor resistor = new Resistor (nodeA, nodeB, componentPosition, resistance);

		Matrix<double> newMNAMatrix = DenseMatrix.Build.DenseOfArray(new double[,] {
			{ 0.5, 1},
			{   1, 0}
		});

		Vector<double> newSourceVector = DenseVector.OfArray (new double[] { 0, 5 });

		Vector<double> newSolutionVector = DenseVector.OfArray (new double[] { 5, -2.5 });

		electronicGridTest.AddComponent (resistor);

		Assert.AreEqual (newMNAMatrix, electronicGridTest.MNAMatrix);
		Assert.AreEqual (newSourceVector, electronicGridTest.sourceVector);
		Assert.AreEqual (newSolutionVector, electronicGridTest.solutionVector);
	}

	[Test]
	public void AddResistorWithNoExistingNodesTest () {
		Node nodeA = new Node (1, 0, 1, 1);
		Node nodeB = new Node (1, 1, 1, 2);
		Position componentPosition = new Position (1, 1);
		double resistance = 2;
		Resistor resistor = new Resistor (nodeA, nodeB, componentPosition, resistance);

		Matrix<double> newMNAMatrix = DenseMatrix.Build.DenseOfArray(new double[,] {
			{  0,    0,    0, 1},
			{  0,  0.5, -0.5, 0},
			{  0, -0.5,  0.5, 0},
			{  1,    0,    0, 0}
		});

		Vector<double> newSourceVector = DenseVector.OfArray (new double[] { 0, 0, 0, 5 });

		Vector<double> newSolutionVector = DenseVector.OfArray (new double[] { 5, 0, 0, 0 });

		electronicGridTest.AddComponent (resistor);

		Assert.AreEqual (newMNAMatrix, electronicGridTest.MNAMatrix);
		Assert.AreEqual (newSourceVector, electronicGridTest.sourceVector);
	}

	[Test]
	public void AddResistorWithOneExistingNodeTest () {
		Node nodeA = new Node (0, 0, 0, 1);
		Node nodeB = new Node (0, 0, 1, 0);
		Position componentPosition = new Position (0, 0);
		double resistance = 2;
		Resistor resistor = new Resistor (nodeA, nodeB, componentPosition, resistance);

		Matrix<double> newMNAMatrix = DenseMatrix.Build.DenseOfArray(new double[,] {
			{  0, 0, 1},
			{  0, 0.5, 0},
			{  1, 0, 0}
		});

		Vector<double> newSourceVector = DenseVector.OfArray (new double[] { 0, 0, 5 });

		Vector<double> newSolutionVector = DenseVector.OfArray (new double[] { 5, 0, 0 });

		electronicGridTest.AddComponent (resistor);

		Assert.AreEqual (newMNAMatrix, electronicGridTest.MNAMatrix);
		Assert.AreEqual (newSourceVector, electronicGridTest.sourceVector);
		Assert.AreEqual (newSolutionVector, electronicGridTest.solutionVector);
	}

	[Test]
	public void AddVoltageSourceWithOneExistingNodeTest () {
		Node nodeA = new Node (0, 0, 0, 1);
		Node nodeB = new Node (0, 0, 1, 0);
		Position componentPosition = new Position (0, 0);
		double voltage = 10;
		VoltageSource voltageSource = new VoltageSource (nodeA, nodeB, componentPosition, voltage);

		Matrix<double> newMNAMatrix = DenseMatrix.Build.DenseOfArray(new double[,] {
			{  0, 0, 1, 0},
			{  0, 0, 0, -1},
			{  1, 0, 0, 1},
			{  0, -1, 1, 0}
		});

		Vector<double> newSourceVector = DenseVector.OfArray (new double[] { 0, 0, 5, 10 });

		Vector<double> newSolutionVector = DenseVector.OfArray (new double[] { 5, 0, 0, 0 });

		electronicGridTest.AddComponent (voltageSource);

		Assert.AreEqual (newMNAMatrix, electronicGridTest.MNAMatrix);
		Assert.AreEqual (newSourceVector, electronicGridTest.sourceVector);
		Assert.AreEqual (newSolutionVector, electronicGridTest.solutionVector);
	}

	[Test]
	public void ClosedCircuitSimpleTest () {
		Node nodeA = new Node (0, 0, 0, 1);
		Node nodeB = new Node (0, 1, 0, 2);
		Node nodeC = new Node (0, 0, 1, 0);
		Node nodeD = new Node (0, 2, 1, 2);
		Node nodeE = new Node (1, 0, 1, 1);
		Node nodeF = new Node (1, 1, 1, 2);

		Position componentPosition1 = new Position (0, 0);
		Position componentPosition2 = new Position (1, 0);
		Position componentPosition3 = new Position (0, 2);
		Position componentPosition4 = new Position (1, 2);
		Position componentPosition5 = new Position (1, 1);

		Wire wire1 = new Wire (nodeA, nodeC, componentPosition1);
		Wire wire2 = new Wire (nodeC, nodeE, componentPosition2);
		Wire wire3 = new Wire (nodeB, nodeD, componentPosition3);
		Wire wire4 = new Wire (nodeD, nodeF, componentPosition4);

		double resistance = 4;
		Resistor resistor = new Resistor (nodeA, nodeB, componentPosition5, resistance);

		electronicGridTest.AddComponent (wire1);
		electronicGridTest.AddComponent (wire2);
		electronicGridTest.AddComponent (wire3);
		electronicGridTest.AddComponent (wire4);
		electronicGridTest.AddComponent (resistor);
	

		Matrix<double> newMNAMatrix = DenseMatrix.Build.DenseOfArray(new double[,] {
			{ 0.25, 1},
			{   1, 0}
		});

		Vector<double> newSourceVector = DenseVector.OfArray (new double[] { 0, 5 });

		Vector<double> newSolutionVector = DenseVector.OfArray (new double[] { 5, -1.25 });

		Assert.AreEqual (newMNAMatrix, electronicGridTest.MNAMatrix);
		Assert.AreEqual (newSourceVector, electronicGridTest.sourceVector);
		Assert.AreEqual (newSolutionVector, electronicGridTest.solutionVector);
	}

	[Test]
	public void ClosedCircuitParallelTest () {
		
		// Simple Closed Circuit

		Node nodeA = new Node (0, 0, 0, 1);
		Node nodeB = new Node (0, 1, 0, 2);
		Node nodeC = new Node (0, 0, 1, 0);
		Node nodeD = new Node (0, 2, 1, 2);
		Node nodeE = new Node (1, 0, 1, 1);
		Node nodeF = new Node (1, 1, 1, 2);

		Position componentPosition1 = new Position (0, 0);
		Position componentPosition2 = new Position (1, 0);
		Position componentPosition3 = new Position (0, 2);
		Position componentPosition4 = new Position (1, 2);
		Position componentPosition5 = new Position (1, 1);

		Wire wire1 = new Wire (nodeA, nodeC, componentPosition1);
		Wire wire2 = new Wire (nodeC, nodeE, componentPosition2);
		Wire wire3 = new Wire (nodeB, nodeD, componentPosition3);
		Wire wire4 = new Wire (nodeD, nodeF, componentPosition4);

		double resistance1 = 2;
		Resistor resistor1 = new Resistor (nodeA, nodeB, componentPosition5, resistance1);

		electronicGridTest.AddComponent (wire1);
		electronicGridTest.AddComponent (wire2);
		electronicGridTest.AddComponent (wire3);
		electronicGridTest.AddComponent (wire4);
		electronicGridTest.AddComponent (resistor1);

		// Parallel Circuit

		Node nodeG = new Node (1, 0, 2, 0);
		Node nodeH = new Node (1, 2, 2, 2);
		Node nodeI = new Node (2, 0, 2, 1);
		Node nodeJ = new Node (2, 1, 2, 2);

		Position componentPosition6 = new Position (2, 0);
		Position componentPosition7 = new Position (2, 2);
		Position componentPosition8 = new Position (2, 1);

		Wire wire5 = new Wire (nodeE, nodeG, componentPosition2);
		Wire wire6 = new Wire (nodeG, nodeI, componentPosition6);
		Wire wire7 = new Wire (nodeF, nodeH, componentPosition4);
		Wire wire8 = new Wire (nodeH, nodeJ, componentPosition7);

		double resistance2 = 2;
		Resistor resistor2 = new Resistor (nodeI, nodeJ, componentPosition8, resistance2);

		electronicGridTest.AddComponent (wire5);
		electronicGridTest.AddComponent (wire6);
		electronicGridTest.AddComponent (wire7);
		electronicGridTest.AddComponent (wire8);
		electronicGridTest.AddComponent (resistor2);

		// Compare with model

		Matrix<double> newMNAMatrix = DenseMatrix.Build.DenseOfArray(new double[,] {
			{ 1, 1},
			{ 1, 0}
		});

		Vector<double> newSourceVector = DenseVector.OfArray (new double[] { 0, 5 });

		Vector<double> newSolutionVector = DenseVector.OfArray (new double[] { 5, -5 });

		Assert.AreEqual (newMNAMatrix, electronicGridTest.MNAMatrix);
		Assert.AreEqual (newSourceVector, electronicGridTest.sourceVector);
		Assert.AreEqual (newSolutionVector, electronicGridTest.solutionVector);
	}

	[Test]
	public void ClosedCircuitSerialTest () {
		Node nodeA = new Node (0, 0, 0, 1);
		Node nodeB = new Node (0, 1, 0, 2);
		Node nodeC = new Node (0, 0, 1, 0);
		Node nodeD = new Node (0, 2, 1, 2);
		Node nodeE = new Node (1, 0, 1, 1);
		Node nodeF = new Node (1, 1, 1, 2);

		Position componentPosition1 = new Position (0, 0);
		Position componentPosition2 = new Position (1, 0);
		Position componentPosition3 = new Position (0, 2);
		Position componentPosition4 = new Position (1, 2);
		Position componentPosition5 = new Position (1, 1);

		Wire wire1 = new Wire (nodeA, nodeC, componentPosition1);
		Wire wire2 = new Wire (nodeC, nodeE, componentPosition2);
		Wire wire3 = new Wire (nodeB, nodeD, componentPosition3);


		double resistance1 = 2;
		Resistor resistor1 = new Resistor (nodeD, nodeF, componentPosition4, resistance1);
		double resistance2 = 2;
		Resistor resistor2 = new Resistor (nodeE, nodeF, componentPosition5, resistance2);

		electronicGridTest.AddComponent (wire1);
		electronicGridTest.AddComponent (wire2);
		electronicGridTest.AddComponent (wire3);
		electronicGridTest.AddComponent (resistor1);
		electronicGridTest.AddComponent (resistor2);


		Matrix<double> newMNAMatrix = DenseMatrix.Build.DenseOfArray(new double[,] {
			{  0.5, -0.5, 1},
			{ -0.5,    1, 0},
			{    1,    0, 0}
		});

		Vector<double> newSourceVector = DenseVector.OfArray (new double[] { 0, 0, 5 });

		Vector<double> newSolutionVector = DenseVector.OfArray (new double[] { 5, 2.5, -1.25 });

		Assert.AreEqual (newMNAMatrix, electronicGridTest.MNAMatrix);
		Assert.AreEqual (newSourceVector, electronicGridTest.sourceVector);
		Assert.AreEqual (newSolutionVector, electronicGridTest.solutionVector);
	}

	[Test]
	public void AddVoltageSourceTest () {
		Node nodeA = new Node (0, 0, 0, 1);
		Node nodeB = new Node (0, 1, 0, 2);
		Node nodeC = new Node (0, 0, 1, 0);
		Node nodeD = new Node (0, 2, 1, 2);
		Node nodeE = new Node (1, 0, 1, 1);
		Node nodeF = new Node (1, 1, 1, 2);

		Position componentPosition1 = new Position (0, 0);
		Position componentPosition2 = new Position (1, 0);
		Position componentPosition3 = new Position (0, 2);
		Position componentPosition4 = new Position (1, 2);
		Position componentPosition5 = new Position (1, 1);

		Wire wire1 = new Wire (nodeA, nodeC, componentPosition1);
		Wire wire2 = new Wire (nodeC, nodeE, componentPosition2);

		double voltage = 5;
		VoltageSource voltageSource = new VoltageSource (nodeB, nodeD, componentPosition3, voltage);
		double resistance1 = 2;
		Resistor resistor1 = new Resistor (nodeD, nodeF, componentPosition4, resistance1);
		double resistance2 = 2;
		Resistor resistor2 = new Resistor (nodeE, nodeF, componentPosition5, resistance2);

		electronicGridTest.AddComponent (wire1);
		electronicGridTest.AddComponent (wire2);
		electronicGridTest.AddComponent (voltageSource);
		electronicGridTest.AddComponent (resistor1);
		electronicGridTest.AddComponent (resistor2);


		Matrix<double> newMNAMatrix = DenseMatrix.Build.DenseOfArray(new double[,] {
			{  0.5, -0.5, 0, 1,-1},
			{ -0.5,    1, 0, 0, 0},
			{    0,    0, 0, 0, 1},
			{    1,    0, 0, 0, 0},
			{   -1,    0, 1, 0, 0}
		});

		Vector<double> newSourceVector = DenseVector.OfArray (new double[] { 0, 0, 0, 5, 5 });

		Vector<double> newSolutionVector = DenseVector.OfArray (new double[] { 5, 5, 10, -2.5, -2.5 });
		Debug.Log (electronicGridTest.solutionVector);
		Assert.AreEqual (newMNAMatrix, electronicGridTest.MNAMatrix);
		Assert.AreEqual (newSourceVector, electronicGridTest.sourceVector);
		Assert.AreEqual (newSolutionVector, electronicGridTest.solutionVector);
	}
}

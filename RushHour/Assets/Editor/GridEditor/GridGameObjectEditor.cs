﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Gamelogic.Grids;

[CustomEditor (typeof(GameObjectEditorComponent))]
public class GridGameObjectEditor : Editor {
	private const float minWidth = 60;
	private int numberOfSpecialButtons;

	private GameObjectEditorComponent grid;
	private List<Texture2D> tiles;

	private float windowWidth;
	private Quaternion originalRotation;

	public void OnEnable() {
		grid = (GameObjectEditorComponent)target;
		GameObject anyCell = grid.gameObject.transform.GetChild (0).gameObject;
		originalRotation = anyCell.transform.rotation;
		SceneView.onSceneGUIDelegate -= OnScene;
		SceneView.onSceneGUIDelegate += OnScene;
		grid.selectedTile = 0;
	}

	public override void OnInspectorGUI() {
		// Hack to get width of Inspector window
		EditorGUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.EndHorizontal();
		EditorGUIUtility.LookLikeControls ();
		Rect scale = GUILayoutUtility.GetLastRect();
		// For some reason scale.width is equal to one on every second call, ignore that.
		if (scale.width != 1) {
			windowWidth = scale.width;
		}

		// Draw palette of drawable items
		EditorGUILayout.BeginHorizontal ();
		EditorGUIUtility.labelWidth = 45;
		grid.tileSet = EditorGUILayout.ObjectField ("Tileset", grid.tileSet, typeof(GameObjectTileSet), false) as GameObjectTileSet;
		// reset labelWidth to default
		EditorGUIUtility.labelWidth = 0;
		EditorGUILayout.EndHorizontal ();
		
		Texture2D cursor = AssetDatabase.LoadAssetAtPath ("Assets/Editor/GridEditor/cursor.png", typeof(Texture2D)) as Texture2D;
		Texture2D delete = AssetDatabase.LoadAssetAtPath ("Assets/Editor/GridEditor/delete.png", typeof(Texture2D)) as Texture2D;
		
		tiles = new List<Texture2D> ();
		tiles.Add (cursor);
		tiles.Add (delete);

		numberOfSpecialButtons = tiles.Count;

		if (grid.tileSet != null) {
			foreach (GameObject go in grid.tileSet.tiles) {
				Texture2D tex2d = AssetPreview.GetAssetPreview (go);
				tiles.Add (tex2d);
			}

			float scrollBarWidth = GUI.skin.verticalScrollbar.fixedWidth;
			int margin = GUI.skin.button.margin.left;
			float scrollAreaWidth = windowWidth -  scrollBarWidth - 2 * margin;
			
			// If the drawing area gets smaller than the icons still draw one item per row
			int itemsPerRow = Mathf.Max((int)(scrollAreaWidth / (minWidth + margin)), 1);
			// Add + 2 to consider cursor and delete button, add itemsPerRow - 1 to round up to the next multiplier
			int numberOfRows = (grid.tileSet.tiles.Count + numberOfSpecialButtons + itemsPerRow - 1) / itemsPerRow;
			
			// Width of entire ScrollArea
			float width = scrollAreaWidth;
			float widthPerItem = (scrollAreaWidth - margin * itemsPerRow) / itemsPerRow;
			
			// Height of entire ScrollArea is numberOfRows, space per button (including margin) and 1 extra margin at side
			float height = numberOfRows * (widthPerItem + margin) + margin;

			EditorGUILayout.BeginHorizontal ();
			int previousSelection = grid.selectedTile;
			grid.selectedTile = GUILayout.SelectionGrid (grid.selectedTile, tiles.ToArray (), itemsPerRow, GUILayout.Width (width), GUILayout.Height (height)); // "toggle");
			if (grid.selectedTile != previousSelection) {
				// When selection was changed set the focus to that of the sceneView
				SceneView sceneView = (SceneView)SceneView.sceneViews [0];
				sceneView.Focus ();
			}
			EditorGUILayout.EndHorizontal();
		}

		EditorGUILayout.BeginHorizontal ();
		EditorGUIUtility.labelWidth = 45;
		grid.layerName = EditorGUILayout.TextField ("Layer", grid.layerName);
		EditorGUILayout.EndHorizontal ();

		SceneView.RepaintAll();
	}

	void OnScene(SceneView sceneview) {
		Event e = Event.current;

		int controlID = GUIUtility.GetControlID (FocusType.Passive);
		EventType eventType = e.GetTypeForControl (controlID);

		Ray r = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x, -e.mousePosition.y + Camera.current.pixelHeight));

		// Detect when game object in scene was touched
		RaycastHit hitInfo;
		GameObject selectedCell = null;
		if (grid.layerName != "") {
			if (Physics.Raycast (r, out hitInfo, Mathf.Infinity, 1 << LayerMask.NameToLayer (grid.layerName))) {
				selectedCell = hitInfo.collider.gameObject;
			}
		} else {
			if (Physics.Raycast (r, out hitInfo)) {
				selectedCell = hitInfo.collider.gameObject;
			}
		}

		// The first two buttons are special for allowing normal manipulation and deletion of objects. React differently for them.
		if (grid.selectedTile >= numberOfSpecialButtons && Selection.activeObject != null 
		   	// Only edit, when grid is selected
		    && Selection.activeGameObject.GetComponent<GameObjectEditorComponent>() != null && Selection.activeGameObject.GetComponent<GameObjectEditorComponent>() == grid) {
			// Get selected object
			int selectedTileIndex = grid.selectedTile - numberOfSpecialButtons;
			GameObject selectedObject = grid.tileSet.tiles [selectedTileIndex];

			if ((eventType == EventType.MouseUp || eventType == EventType.MouseDrag) && selectedCell != null) {
				if (PrefabUtility.GetPrefabParent (selectedObject) == null && PrefabUtility.GetPrefabObject (selectedObject) != null) {
					GameObject mesh = selectedCell.transform.FindChild("Mesh").gameObject;
					GameObject meshParent = mesh.transform.parent.gameObject;
					MeshCellData selectedMeshCellData = meshParent.GetComponent<MeshCellData>();
					if (selectedMeshCellData.meshID == selectedTileIndex && eventType == EventType.MouseUp) {
						mesh.transform.RotateAround(meshParent.GetComponent<Collider>().bounds.center, new Vector3(0, 1, 0), 90);
					} else {
						while (mesh.transform.eulerAngles.y % 360 != 0) {
							mesh.transform.RotateAround(meshParent.GetComponent<Collider>().bounds.center, new Vector3(0, 1, 0), 90);
						}

						selectedMeshCellData.meshID = selectedTileIndex;

						List<Transform> tempList = mesh.transform.Cast<Transform>().ToList();
						foreach(Transform childTransform in tempList) {
							Undo.DestroyObjectImmediate(childTransform.gameObject);
						}
						// Check that activeObject is a prefab
						GameObject obj = (GameObject) PrefabUtility.InstantiatePrefab (selectedObject);
						obj.transform.parent = mesh.transform;
						Undo.RegisterCreatedObjectUndo (obj, "Create " + obj.name);
						obj.transform.position = mesh.transform.position;
					}
				}
			}

			if (eventType == EventType.MouseUp) {
				GUIUtility.hotControl = controlID;
				e.Use ();
			} else if (eventType == EventType.MouseDrag) {
				e.Use ();
			} else if (eventType == EventType.MouseDown) {
				GUIUtility.hotControl = 0;
				e.Use ();
				Undo.IncrementCurrentGroup ();
			}

		}
		// Delete Game Objects in those places
		if (grid.selectedTile == 1) {
			if (eventType == EventType.MouseUp || eventType == EventType.MouseDrag) {
				GameObject mesh = selectedCell.transform.FindChild("Mesh").gameObject;
				List<Transform> tempList = mesh.transform.Cast<Transform>().ToList();
				foreach(Transform childTransform in tempList) {
					Undo.DestroyObjectImmediate(childTransform.gameObject);
				}
				e.Use();
			}
		}
	}

}
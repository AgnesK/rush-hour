﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Gamelogic.Grids;

[CustomEditor (typeof(SpriteEditorComponent))]
public class GridSpriteEditor : Editor {
	private const float minWidth = 60;
	private int numberOfSpecialButtons;

	private SpriteEditorComponent grid;
	private List<Texture2D> tiles;

	private float windowWidth;

	public void OnEnable() {
		grid = (SpriteEditorComponent)target;
		SceneView.onSceneGUIDelegate -= OnScene;
		SceneView.onSceneGUIDelegate += OnScene;
		grid.selectedTile = 0;
	}

	public override void OnInspectorGUI() {
		// Hack to get width of Inspector window
		EditorGUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.EndHorizontal();
		EditorGUIUtility.LookLikeControls ();
		Rect scale = GUILayoutUtility.GetLastRect();
		// For some reason scale.width is equal to one on every second call, ignore that.
		if (scale.width != 1) {
			windowWidth = scale.width;
		}
		
		Texture2D cursor = AssetDatabase.LoadAssetAtPath ("Assets/Editor/GridEditor/cursor.png", typeof(Texture2D)) as Texture2D;
//		Texture2D delete = AssetDatabase.LoadAssetAtPath ("Assets/Editor/GridEditor/delete.png", typeof(Texture2D)) as Texture2D;
//		tiles.Add (delete);
		
		tiles = new List<Texture2D> ();
		tiles.Add (cursor);

		numberOfSpecialButtons = tiles.Count;

		SpriteCell spriteCell = (SpriteCell) grid.gameObject.GetComponent<TileGridBuilder<RectPoint>>().CellPrefab;

		if (spriteCell != null) {
			Sprite[] availableTiles = spriteCell.sprites;
			foreach (Sprite gameObject in availableTiles) {
				Texture2D tex2d = AssetPreview.GetAssetPreview (gameObject);
				tiles.Add (tex2d);
			}

			float scrollBarWidth = GUI.skin.verticalScrollbar.fixedWidth;
			int margin = GUI.skin.button.margin.left;
			float scrollAreaWidth = windowWidth -  scrollBarWidth - 2 * margin;
			
			// If the drawing area gets smaller than the icons still draw one item per row
			int itemsPerRow = Mathf.Max((int)(scrollAreaWidth / (minWidth + margin)), 1);
			// Add + 2 to consider cursor and delete button, add itemsPerRow - 1 to round up to the next multiplier
			int numberOfRows = (availableTiles.Length + numberOfSpecialButtons + itemsPerRow - 1) / itemsPerRow;
			
			// Width of entire ScrollArea
			float width = scrollAreaWidth;
			float widthPerItem = (scrollAreaWidth - margin * itemsPerRow) / itemsPerRow;
			
			// Height of entire ScrollArea is numberOfRows, space per button (including margin) and 1 extra margin at side
			float height = numberOfRows * (widthPerItem + margin) + margin;

			EditorGUILayout.BeginHorizontal ();
			int previousSelection = grid.selectedTile;
			grid.selectedTile = GUILayout.SelectionGrid (grid.selectedTile, tiles.ToArray (), itemsPerRow, GUILayout.Width (width), GUILayout.Height (height)); // "toggle");
			if (grid.selectedTile != previousSelection) {
				// When selection was changed set the focus to that of the sceneView
				SceneView sceneView = (SceneView)SceneView.sceneViews [0];
				sceneView.Focus ();
			}
			EditorGUILayout.EndHorizontal();
		}
		
		SceneView.RepaintAll();
	}

	void OnScene(SceneView sceneview) {
		Event e = Event.current;

		int controlID = GUIUtility.GetControlID (FocusType.Passive);
		EventType eventType = e.GetTypeForControl (controlID);

		Ray r = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x, -e.mousePosition.y + Camera.current.pixelHeight));
		// Detect when game object in scene was touched
		RaycastHit hitInfo;
		GameObject selectedCell = null;

		if (Physics.Raycast (r, out hitInfo, Mathf.Infinity, 1 << LayerMask.NameToLayer("Electronics"))) {
			selectedCell = hitInfo.collider.gameObject;
		}

		// The first two buttons are special for allowing normal manipulation and deletion of objects. React differently for them.
		if (grid != null && grid.selectedTile >= numberOfSpecialButtons && Selection.activeObject != null 
		// Only edit, when grid is selected
			&& Selection.activeGameObject.GetComponent<SpriteEditorComponent> () != null && Selection.activeGameObject.GetComponent<SpriteEditorComponent> () == grid) {
			if (eventType == EventType.MouseDrag || eventType == EventType.MouseUp) {
				if (selectedCell != null) {
					Undo.RegisterUndo (selectedCell, "Edit " + selectedCell);
					// Correct for two special buttons by substracting numberOfSpecialButtons
					SpriteCell spriteCell = selectedCell.transform.parent.gameObject.GetComponent<SpriteCell> ();
					spriteCell.FrameIndex = grid.selectedTile - numberOfSpecialButtons;
				}
			}
			if (eventType == EventType.MouseUp) {
				GUIUtility.hotControl = controlID;
				e.Use ();
			} else if (eventType == EventType.MouseDrag) {
				e.Use ();
			} else if (eventType == EventType.MouseDown) {
				Undo.IncrementCurrentGroup ();
				GUIUtility.hotControl = 0;
				e.Use ();
			}
		}
	}

	private SpriteCell getSelectedCell(RaycastHit2D hitInfo2D) {
		if (hitInfo2D.collider != null) {
			return hitInfo2D.collider.gameObject.transform.parent.gameObject.GetComponent<SpriteCell>();
		}
		return null;
	}

}
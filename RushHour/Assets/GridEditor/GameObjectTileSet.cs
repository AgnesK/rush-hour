﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

// Helpful for Tileset of sprites, so they can be exchanged, with GameObjects this is not necessary, just edit original GameObject
//[Serializable] public class TileSet : SerializableDictionary<int, GameObject> { }
//[CustomPropertyDrawer(typeof(TileSet))]
//public class TileSetDrawer : DictionaryDrawer<int, GameObject> { }

public class GameObjectTileSet : ScriptableObject {
	public List<GameObject> tiles;
	
	#if UNITY_EDITOR
	[MenuItem("Assets/Create/GridTileSet")]
	/**
	 * Create GridTileSet in current directory
	 */
	public static void CreateTileSet() {
		string currentPath = getCurrentPath ();

		GameObjectTileSet gridTileSet = CreateInstance<GameObjectTileSet>();
		AssetDatabase.CreateAsset(gridTileSet, currentPath + "/GridTileSet.asset");
		AssetDatabase.SaveAssets();
	}

	/**
	 * Find current path selected in Project window
	 */
	private static string getCurrentPath() {
		string path = "Assets";
		foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
		{
			path = AssetDatabase.GetAssetPath(obj);
			if (File.Exists(path)) {
				path = Path.GetDirectoryName(path);
			}
			break;
		}
		return path;
	}



	#endif

}
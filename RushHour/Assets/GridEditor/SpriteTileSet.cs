﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class SpriteTileSet : ScriptableObject {
	public List<Sprite> tileSet;
	
	#if UNITY_EDITOR
	[MenuItem("Assets/Create/SpriteTileSet")]
	/**
	 * Create GridTileSet in current directory
	 */
	public static void CreateTileSet() {
		string currentPath = getCurrentPath ();
		
		SpriteTileSet gridTileSet = CreateInstance<SpriteTileSet>();
		AssetDatabase.CreateAsset(gridTileSet, currentPath + "/SpriteTileSet.asset");
		AssetDatabase.SaveAssets();
	}
	
	/**
	 * Find current path selected in Project window
	 */
	private static string getCurrentPath() {
		string path = "Assets";
		foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
		{
			path = AssetDatabase.GetAssetPath(obj);
			if (File.Exists(path)) {
				path = Path.GetDirectoryName(path);
			}
			break;
		}
		return path;
	}
	
	
	
	#endif
	
}
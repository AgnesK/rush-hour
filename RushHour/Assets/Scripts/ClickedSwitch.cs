﻿using UnityEngine;
using System.Collections;

public class ClickedSwitch : MonoBehaviour {

	public Sprite openSwitch;
	public Sprite closedSwitch;
	public bool open = true;
	public GameObject target;

	void OnMouseUp() {
		Debug.Log ("click");
		if (open) {
//			target.GetComponent<SwitchToggled> ().SwitchLightOn();
//			SetSpriteRendererAlpha (openSwitch, 0.0f);
//			SetSpriteRendererAlpha (closedSwitch, 1.0f);
			open = false;
		} else {
//			target.GetComponent<SwitchToggled> ().SwitchLightOff();
//			SetSpriteRendererAlpha (openSwitch, 1.0f);
//			SetSpriteRendererAlpha (closedSwitch, 0.0f);
			open = true;
		}
	}

	void SetSpriteRendererAlpha(SpriteRenderer spriteRenderer, float alpha) {
		Color currentColor = spriteRenderer.color;
		currentColor.a = alpha;
		spriteRenderer.color = currentColor;
	}
}

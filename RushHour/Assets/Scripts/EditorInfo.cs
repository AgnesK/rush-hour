﻿using UnityEngine;
using System.Collections;

public enum ButtonType { None, Wire, Switch, Resistor, Condensator, Diode, Transistor, Delete }

public class EditorInfo : MonoBehaviour {
	
	private ButtonType currentlySelected;
	public ButtonType CurrentlySelected {
		get { return currentlySelected; }
		set { currentlySelected = value; }
	}

	public void SetSelectedButton(string buttonType) {
		switch (buttonType) {
		case "Wire":
			currentlySelected = ButtonType.Wire;
			break;
		case "Switch":
			currentlySelected = ButtonType.Switch;
			break;
		case "Resistor":
			currentlySelected = ButtonType.Resistor;
			break;
		case "Condensator":
			currentlySelected = ButtonType.Condensator;
			break;
		case "Diode":
			currentlySelected = ButtonType.Diode;
			break;
		case "Transistor":
			currentlySelected = ButtonType.Transistor;
			break;
		case "Delete":
			currentlySelected = ButtonType.Delete;
			break;
		default:
			currentlySelected = ButtonType.None;
			break;
		}
	}

}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

public class ElectronicComponent {

	public Position position;

	public ElectronicComponent () {}

	public ElectronicComponent (Position position) {
		this.position = position;
	}

	public virtual void updateModel (ElectronicGrid electronicGrid) {}
}

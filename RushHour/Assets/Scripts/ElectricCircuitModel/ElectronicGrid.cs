﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

public class ElectronicGrid : MonoBehaviour {

	private ElectronicComponent[ , ] electronicGrid;
//	private List< List<Edge> > cycles;
	private int levelWidth = 7;
	private int levelHeight = 7;

	public List< List<Node>> equivalentNodes = new List< List<Node>> ();
	public List< VoltageSource> voltageSources = new List<VoltageSource>();

	// The MNA Matrix is divided into four matrices MNAMatrix A = [G, B; C, D]
	public Matrix<double> MNAMatrix;
	// Solution Vector with voltages and currents; z = [v, j]
	public Vector<double> solutionVector;
	// Independent Sources Vector with sum of currents through passive elements (often zero) and voltage sources; z = [i, e]
	public Vector<double> sourceVector;

	public ElectronicGrid () {
		electronicGrid = new ElectronicComponent[levelHeight, levelWidth];

		Node ground = new Node (0, 0, 0, 1);
		ground.addNode (equivalentNodes);

		Position newPosition = new Position (0, 1);
		VoltageSource newVoltageSource = new VoltageSource (ground, new Node(0,1,0,2), newPosition, 5);
		AddComponent (newVoltageSource);
		voltageSources.Add (newVoltageSource);
	}

	public void AddComponent (ElectronicComponent component) {
		// Is there already a component?

			// Is it a Wire?

		int x = component.position.row;
		int y = component.position.column;
		electronicGrid [x, y] = component;

		component.updateModel (this);
		SolveModelEquations ();
	}

	public void DeleteComponent (int x, int y) {
		ElectronicComponent component = electronicGrid [x, y];
		electronicGrid [x, y] = null;

		// TODO: delete component.node1
		// TODO: delete component.node2
	}

	public void SolveModelEquations () {
		solutionVector = MNAMatrix.Solve (sourceVector);
	}
}

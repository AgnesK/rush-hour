﻿using System;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

public static class MatrixManipulations {

	public static bool isMatrixEmpty (Matrix<double> A) {
		return (A == null);
	}
	
	public static Matrix<double> extendMatrix (Matrix<double> A, int i) {
		if (A == null) {
			A = DenseMatrix.Build.Dense (1,1);
		} else {
			int dimension = A.RowCount;
			Vector<double> zeros = DenseVector.Build.Dense (dimension);
			A = A.InsertColumn(i, zeros);

			dimension = A.ColumnCount;
			zeros = DenseVector.Build.Dense (dimension);
			A = A.InsertRow(i, zeros);
		}

		return A;
	}

	public static Matrix<double> reduceMatrix (Matrix<double> A, int i) {
		A = A.RemoveColumn (i);
		A = A.RemoveRow (i);
		return A;
	}

	public static Matrix<double> addValueToMatrix (Matrix<double> A, int i, double value) {
		double temp = A.At (i, i);
		A.At (i, i, temp + value);
		return A;
	}

	public static Matrix<double> addTwoValuesToMatrix (Matrix<double> A, int i, int j, double value) {
		A.At (i, j, value);
		A.At (j, i, value);

		return A;
	}

	public static Matrix<double> addMultipleValuesToMatrix (Matrix<double> A, int i, int j, double value) {
		double temp = A.At (i, i);
		A.At (i, i, temp + value);

		temp = A.At (j, j);
		A.At (j, j, temp + value);

		temp = A.At (i, j);
		A.At (i, j, temp - value);

		temp = A.At (j, i);
		A.At (j, i, temp - value);

		return A;
	}

	public static Vector<double> extendVector (Vector<double> v, int i) {
		Vector<double> w;
		if (v == null) {
			w = DenseVector.Build.Dense (1);
		} else {
			int length = v.Count;
			w = DenseVector.Create (length + 1, 0);
			v.CopySubVectorTo (w, 0, 0, i);
			v.CopySubVectorTo (w, i, i + 1, length - i);
		}

		return w;
	}
}
	
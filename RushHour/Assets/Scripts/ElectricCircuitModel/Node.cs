﻿using MathNet.Numerics;
using System.Collections.Generic;
using System;

public class Node
{
	public Tuple <Position, Position> position;

	public Node (Position position1, Position position2) {
		this.position = new Tuple<Position, Position> (position1, position2);
	}

	public Node (int xPosition1, int yPosition1, int xPosition2, int yPosition2) {
		Position position1 = new Position(xPosition1, yPosition1);
		Position position2 = new Position(xPosition2, yPosition2);
		this.position = new Tuple<Position, Position> (position1, position2);
	}

	public override bool Equals (System.Object obj) {
		if (obj == null) return false;

		Node node = (Node)obj;
		return (this.position.Item1.Equals(node.position.Item1) && this.position.Item2.Equals(node.position.Item2));
	}

	public bool nodeAlreadyExists(List< List<Node>> equivalentNodes) {
		foreach (List<Node> nodes in equivalentNodes) {
			foreach (Node node in nodes) {
				if (node.Equals(this)) return true;
			}
		}
		return false;
	}

	public int findNode (List< List<Node>> equivalentNodes) {
		int nodeIndex = -1;
		for (int i=0; i < equivalentNodes.Count; i++) {
			foreach (Node node in equivalentNodes[i]) {
				if (node.Equals (this)) {
					nodeIndex = i;
				}
			}
		}
		return nodeIndex;
	}

	public int addNode (List< List<Node>> equivalentNodes) {
		int nodeIndex = this.findNode (equivalentNodes);

		if (nodeIndex == -1) {
			List<Node> nodeList = new List<Node> ();
			nodeList.Add (this);
			equivalentNodes.Add (nodeList);
		}

		return nodeIndex;
	}

	public void deleteNode () {

	}
}

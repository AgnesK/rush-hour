﻿using System;
using UnityEngine;

public class NonWire : ElectronicComponent {
	
	public Node node1;
	public Node node2;

	protected int nodeIndex1;
	protected int nodeIndex2;

	public NonWire (Node node1, Node node2, Position position) : base(position) {
		this.node1 = node1;
		this.node2 = node2;
	}

	public override void updateModel (ElectronicGrid electronicGrid) {
		nodeIndex1 = node1.addNode(electronicGrid.equivalentNodes);
		if (nodeIndex1 == -1) {
			nodeIndex1 = electronicGrid.equivalentNodes.Count -1;
			electronicGrid.MNAMatrix = MatrixManipulations.extendMatrix (electronicGrid.MNAMatrix, nodeIndex1 -1);
			electronicGrid.sourceVector = MatrixManipulations.extendVector (electronicGrid.sourceVector, nodeIndex1 -1);
		}

		nodeIndex2 = node2.addNode(electronicGrid.equivalentNodes);
		if (nodeIndex2 == -1) {
			nodeIndex2 = electronicGrid.equivalentNodes.Count -1;
			electronicGrid.MNAMatrix = MatrixManipulations.extendMatrix (electronicGrid.MNAMatrix, nodeIndex2 -1);
			electronicGrid.sourceVector = MatrixManipulations.extendVector (electronicGrid.sourceVector, nodeIndex2 -1);
		}
	}
}

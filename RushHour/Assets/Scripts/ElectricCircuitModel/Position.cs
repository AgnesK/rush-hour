﻿using System;

[System.Serializable]
public class Position {
	public int row;
	public int column;

	public Position (int xPosition, int yPosition) {
		row = xPosition;
		column = yPosition;
	}

	public override bool Equals (System.Object obj) {
		if (obj == null) return false;

		Position position = (Position)obj;
		return position.row == this.row && position.column == this.column;
	}
}

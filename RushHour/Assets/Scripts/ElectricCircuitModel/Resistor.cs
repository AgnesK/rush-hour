﻿using UnityEngine;
using System;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

public class Resistor : NonWire {

	double resistance;

	public Resistor (Node node1, Node node2, Position position, double resistance) : base(node1, node2, position) {
		this.resistance = resistance;
	}

	public override void updateModel (ElectronicGrid electronicGrid) {
		Debug.Log ("I am a Resistor");
		base.updateModel (electronicGrid);
		if (nodeIndex1 == 0) {
			// one of the nodes is directly connected to ground
			electronicGrid.MNAMatrix = MatrixManipulations.addValueToMatrix (electronicGrid.MNAMatrix, nodeIndex2 -1, 1/resistance);
		} else if (nodeIndex2 == 0) {
			electronicGrid.MNAMatrix = MatrixManipulations.addValueToMatrix (electronicGrid.MNAMatrix, nodeIndex1 -1, 1/resistance);
		} else {
			electronicGrid.MNAMatrix = MatrixManipulations.addMultipleValuesToMatrix (electronicGrid.MNAMatrix, nodeIndex1 -1, nodeIndex2 -1, 1/resistance);
		}
	}
		
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

public class Switch : ElectronicComponent {

	public bool isOpen;
//	public float openResistance = 1000000000.0f;
//	public float closedResistance = 0.0017f;

	public Switch(Node node1, Node node2, Position position) : base(position) {
		isOpen = true;
//
//		Edge newEdge = new Edge ();
//		newEdge.start = Vertex.Left;
//		newEdge.end = Vertex.Right;
//		newEdge.resistance = openResistance;
//		newEdge.isBroken = false;
//		edgeArray = new Edge[] {newEdge};
	}

//	public void toggleSwitch() {
//		isOpen = !isOpen;
//		if (isOpen) {
//			edgeArray [1].resistance = openResistance;
//		} else {
//			edgeArray [1].resistance = closedResistance;
//		}
//
//		//TODO: Change Prefab picture and check for closed circuits
//	}


	public override void updateModel (ElectronicGrid electronicGrid) {
		Debug.Log ("I am a Switch");
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

public class VoltageSource : NonWire {

	public double voltage;
//	public Vertex positiveConnection;
//
	public VoltageSource(Node node1, Node node2, Position position, double voltage) : base(node1, node2, position) {
		this.voltage = voltage;
	}

	public override void updateModel (ElectronicGrid electronicGrid) {
		Debug.Log ("I am a Voltage Source");
		base.updateModel (electronicGrid);
		electronicGrid.voltageSources.Add (this);

		int matrixIndex = electronicGrid.equivalentNodes.Count -1 + electronicGrid.voltageSources.Count -1;
		electronicGrid.MNAMatrix = MatrixManipulations.extendMatrix (electronicGrid.MNAMatrix, matrixIndex);
		Debug.Log (matrixIndex);
		Debug.Log (nodeIndex1);
		Debug.Log (nodeIndex2);
		if (nodeIndex1 == 0) {
			// one of the nodes is directly connected to ground
			electronicGrid.MNAMatrix = MatrixManipulations.addTwoValuesToMatrix (electronicGrid.MNAMatrix, matrixIndex, nodeIndex2 -1, 1);
		} else if (nodeIndex2 == 0) {
			electronicGrid.MNAMatrix = MatrixManipulations.addTwoValuesToMatrix (electronicGrid.MNAMatrix, matrixIndex, nodeIndex1 -1, -1);
		} else {
			electronicGrid.MNAMatrix = MatrixManipulations.addTwoValuesToMatrix (electronicGrid.MNAMatrix, matrixIndex, nodeIndex1 -1, -1);
			electronicGrid.MNAMatrix = MatrixManipulations.addTwoValuesToMatrix (electronicGrid.MNAMatrix, matrixIndex, nodeIndex2 -1, 1);
		}

		electronicGrid.sourceVector = MatrixManipulations.extendVector (electronicGrid.sourceVector, matrixIndex);
		electronicGrid.sourceVector.At (matrixIndex, voltage);
	}
}

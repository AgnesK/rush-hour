﻿using UnityEngine;
using System;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

public class Wire : ElectronicComponent {

	List <List<Node>> equivalentWireNodes = new List< List<Node>>();

	public Wire (Node node1, Node node2, Position position) : base(position) {
		List<Node> newNodes = new List<Node>();
		newNodes.Add(node1);
		newNodes.Add(node2);
		equivalentWireNodes.Add(newNodes);
	}

	public override void updateModel (ElectronicGrid electronicGrid) {
		Debug.Log ("I am a Wire");
		foreach (List<Node> nodeList in equivalentWireNodes) {
			int resultingNodeIndex = -1;
			List<Node> newNodeList = new List<Node>();
			foreach (Node newNode in nodeList) {
				int newNodeIndex = newNode.findNode (electronicGrid.equivalentNodes);
				if (newNodeIndex == -1) {
					newNodeList.Add (newNode);
				} else {
					if (resultingNodeIndex == -1) {
						resultingNodeIndex = newNodeIndex;
					} else if (resultingNodeIndex == newNodeIndex) {
						// nothing to do :)
					} else {
						// MERGE LISTS resultingNodeIndex AND newNodeIndex
						// UPDATE MATRIX!
					}
				}
			}
			if (resultingNodeIndex == -1) {
				electronicGrid.equivalentNodes.Add (newNodeList);
				resultingNodeIndex = electronicGrid.equivalentNodes.Count - 1;
				electronicGrid.MNAMatrix = MatrixManipulations.extendMatrix (electronicGrid.MNAMatrix, resultingNodeIndex - 1);
				electronicGrid.sourceVector = MatrixManipulations.extendVector (electronicGrid.sourceVector, resultingNodeIndex - 1);
			} else {
				electronicGrid.equivalentNodes [resultingNodeIndex].AddRange (newNodeList);
			}

		}
	}

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ElectronicComponentsMapping {
	public ButtonType type;
	public GameObject typeObject;
}
	
public class ElectronicCellBehavior : MonoBehaviour {
	public bool isInEditMode;
	private EditorInfo editorInfo;
	public ElectronicComponentsMapping[] electronicMapping;

	private Vector3 entryPosition;
	private bool mouseTouches;
	public float tolerance = 4;

	Vector2 entryDirection = Vector2.zero;
	Vector2 exitDirection = Vector2.zero;

	void Start() {
		editorInfo = GameObject.Find ("GameController").GetComponent<EditorInfo> ();
	}

	private void setGameObject(GameObject mesh, ElectronicComponentsMapping mapping) {
		// Set new game object
		GameObject createdObject = Instantiate(mapping.typeObject);
		createdObject.transform.SetParent (mesh.transform);
		createdObject.transform.parent = mesh.transform;
		createdObject.transform.position = mesh.transform.position;
	}

	void Update() {
		GameObject mesh = gameObject.transform.FindChild("Mesh").gameObject;
		Ray r = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y));
		RaycastHit hitInfo;
		// Get direction of new wire
		if (Physics.Raycast (r, out hitInfo, Mathf.Infinity, 1 << LayerMask.NameToLayer ("Electronics"))) {
			if (hitInfo.collider.gameObject == gameObject) {
				if ((Input.GetMouseButtonDown (0) || Input.GetMouseButton (0)) && !mouseTouches) {
					mouseTouches = true;
					entryPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
					entryDirection = DirectionInCell (entryPosition);

					// if there isn't a wire at the current position, make sure to put one
					foreach (ElectronicComponentsMapping mapping in electronicMapping) {
						if (editorInfo.CurrentlySelected == ButtonType.Wire && mapping.type == ButtonType.Wire) {
							Transform electronicComponent = mesh.transform.GetChild(0);
							// if there currently is no wire, make the component a wire
							if (electronicComponent.gameObject.tag != mapping.typeObject.tag) {
								// Delete current child of mesh
								Destroy (electronicComponent.gameObject);
								setGameObject (mesh, mapping);
							}
							break;
						}
					}
				}
				if (Input.GetMouseButtonUp (0) && mouseTouches) {
					mouseTouches = false;
					Vector3 exitPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
					exitDirection = DirectionInCell (exitPosition);
					WireVisualizer wireVisualizer = mesh.transform.GetChild(0).GetComponent<WireVisualizer>();
					wireVisualizer.SetView (entryDirection, exitDirection);
				}
			} else {
				if (Input.GetMouseButton (0) && mouseTouches) {
					mouseTouches = false;
					Vector3 exitPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
					exitDirection = DirectionInCell (exitPosition);
					WireVisualizer wireVisualizer = mesh.transform.GetChild(0).GetComponent<WireVisualizer>();
					wireVisualizer.SetView (entryDirection, exitDirection);
				}
			}
		}
	}

	private void handleInput() {
		if (isInEditMode) {
			foreach (ElectronicComponentsMapping mapping in electronicMapping) {
				if (mapping.type == editorInfo.CurrentlySelected) {
					GameObject mesh = gameObject.transform.FindChild("Mesh").gameObject;

					Transform electronicComponent = mesh.transform.GetChild(0);
					// Rotate if there is already a game object of the same type
					if (electronicComponent.gameObject.tag == mapping.typeObject.tag) {
						electronicComponent.Rotate (new Vector3 (0, 0, -90));
						// TODO: adjust the ElectronicComponent so that power flow is fit to new orientation
					} else {
						// Delete current child of mesh
						Destroy (electronicComponent.gameObject);
						setGameObject (mesh, mapping);
					}
				}
			}
		} else {
			ElectronicComponentBehavior behavior = gameObject.GetComponentInChildren<ElectronicComponentBehavior> ();
			if (behavior != null) {
				behavior.TriggerComponent ();
			}
		}
	}

	private Vector2 DirectionInCell(Vector3 exitPosition) {
		Vector2 direction = Vector2.zero;
		BoxCollider collider = gameObject.GetComponent<BoxCollider> ();

		float distanceToLeft = exitPosition.x - collider.bounds.min.x;
		float distanceToRight = collider.bounds.max.x - exitPosition.x;
		if (distanceToLeft < 4) {
			direction.x = -1;
		} else if (distanceToRight < tolerance) {
			direction.x = 1;
		}
		float distanceToTop = collider.bounds.max.z - exitPosition.z;
		float distanceToBottom = exitPosition.z - collider.bounds.min.z;
		if (distanceToTop < 4) {
			direction.y = -1;
		} else if (distanceToBottom < tolerance) {
			direction.y = 1;
		}
		return direction;
	}

	private GameObject getElectronicComponent(ButtonType buttonType) {
		foreach (ElectronicComponentsMapping mapping in electronicMapping) {
			if (mapping.type == buttonType) {
				return mapping.typeObject;
			}
		}
		return null;
	}

}

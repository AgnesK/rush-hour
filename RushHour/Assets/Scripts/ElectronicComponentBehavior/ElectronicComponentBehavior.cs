﻿using UnityEngine;
using System.Collections;

public abstract class ElectronicComponentBehavior : MonoBehaviour {

	public virtual void TriggerComponent () {
		Debug.Log ("No custom behavior for this component");
	}

}
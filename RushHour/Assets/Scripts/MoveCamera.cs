﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour {
	[SerializeField]
	private Transform editModeTransform;
	[SerializeField]
	private Transform playModeTransform;

	private ToggleMode.Mode currentMode = ToggleMode.Mode.Edit;

	private float startTime;
	public float animationDuration;

	public void ToggleCamera(ToggleMode.Mode newMode) {
		currentMode = newMode;
		startTime = Time.time;
	}

	void Update () {
		float timeInterval = Time.time - startTime;
		if (currentMode == ToggleMode.Mode.Edit && editModeTransform != gameObject.transform) {
			transform.position = Vector3.Lerp (gameObject.transform.position, editModeTransform.position, timeInterval / animationDuration);
			transform.rotation = Quaternion.Lerp (gameObject.transform.rotation, editModeTransform.rotation, timeInterval / animationDuration);
		} else if (currentMode == ToggleMode.Mode.Play && playModeTransform != gameObject.transform) {
			transform.position = Vector3.Lerp (gameObject.transform.position, playModeTransform.position, timeInterval / animationDuration);
			transform.rotation = Quaternion.Lerp (gameObject.transform.rotation, playModeTransform.rotation, timeInterval / animationDuration);
		}
	}

}

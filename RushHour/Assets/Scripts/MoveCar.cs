﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveCar : MonoBehaviour {
//	[HideInInspector]
	public GameObject[] waypoints;
	private int currentWaypoint = 0;
	public float speed = 1.0f;
	public bool redLightInProximity;

	private bool collided;
	private bool arrivedAndHonked;

	private List<AudioSource> audioSources;

	// Use this for initialization
	void Start () {
		RotateIntoMoveDirection();
		gameObject.transform.position = waypoints [0].transform.position;

		audioSources = new List<AudioSource> ();
		foreach (AudioSource audioSource in GetComponents<AudioSource>()) {
			audioSources.Add(audioSource);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!redLightInProximity) {
			if (!collided) {

				Vector3 currentPosition = gameObject.transform.position;
				Vector3 endPosition = waypoints [currentWaypoint + 1].transform.position;

				Vector3 walkVector = endPosition - currentPosition;
				Vector3 normalizedWalkVector = Vector3.Normalize (walkVector);
				Vector3 actualWalkVector = normalizedWalkVector * speed * Time.deltaTime;

				if (actualWalkVector.magnitude > walkVector.magnitude) {
					actualWalkVector = walkVector;
				}
				gameObject.transform.position += actualWalkVector;

				// Tolerance of .001 is required, as positions are never truly identical
				if (gameObject.transform.position == endPosition) {
					if (currentWaypoint < waypoints.Length - 2) {
						currentWaypoint++;
						RotateIntoMoveDirection ();
					} else {
						foreach (AudioSource audioSource in audioSources) {
							if (audioSource.priority == 0) {
								audioSource.Play();
							}
						}
						StartCoroutine(DestroyCar());
					}
				}
			}
		}
	}

	private IEnumerator DestroyCar() {
		// Car arrived successfully, maybe play sound to warrant this
		foreach (AudioSource audioSource in audioSources) {
			if (audioSource.priority == 0) {
				audioSource.Play();
				yield return new WaitForSeconds(audioSource.clip.length);
			}
		}
		Destroy (gameObject);
	}

	private void RotateIntoMoveDirection() {
		Vector3 newStartPosition = waypoints [currentWaypoint].transform.position;
		Vector3 newEndPosition = waypoints [currentWaypoint + 1].transform.position;
		Vector3 newDirection = (newEndPosition - newStartPosition);
		gameObject.transform.rotation = Quaternion.LookRotation (-newDirection);
	}

	public float distanceToGoal() {
		float distance = 0;
		distance += Vector3.Distance (gameObject.transform.position, waypoints [currentWaypoint + 1].transform.position);
		for (int i = currentWaypoint + 1; i < waypoints.Length - 1; i++) {
			Vector3 startPosition = waypoints [i].transform.position;
			Vector3 endPosition = waypoints [i + 1].transform.position;
			distance += Vector3.Distance(startPosition, endPosition);
		}
		return distance;
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "Car") {
			collided = true;
			Collider collider = gameObject.GetComponent<Collider>();
			foreach (AudioSource audioSource in audioSources) {
				if (audioSource.priority == 10) {
					audioSource.Play();
				}
			}
		}
	}

}

﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Wave {
	public GameObject enemyPrefab;
	public float spawnInterval = 2;
	public int maxEnemies = 20;
}

public class SpawnEnemy : MonoBehaviour {
	public GameObject[] waypoints;

	public Wave[] waves;
	public int timeBetweenWaves = 5;
	
	private float lastSpawnTime;
	private int enemiesSpawned = 0;

	public float timeBeforeFirstWave = 10;
	public bool wavesWaitForPreviousWaves = true;

	// Use this for initialization
	void Start () {
		lastSpawnTime =  Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		// 1 
		float timeInterval = Time.time - lastSpawnTime;
		int currentWave = 0;
		// 2 
		if (currentWave < waves.Length) {
			// 3 
			float spawnInterval = waves[currentWave].spawnInterval;
			// Wait longer for first wave
			if (currentWave == 0 && enemiesSpawned == 0 && timeInterval > timeBeforeFirstWave) {
				SpawnNext();
			// Between waves wait for timeBetweenWaves and spawnInterval
			} else if (currentWave != 0 || enemiesSpawned != 0) {
				if (((enemiesSpawned == 0 && timeInterval > timeBetweenWaves) || timeInterval > spawnInterval)
			    // Is wave finishes?
			    && enemiesSpawned < waves[currentWave].maxEnemies) {
					SpawnNext();
				}
			}
			// 4 
			if (enemiesSpawned == waves[currentWave].maxEnemies && (GameObject.FindGameObjectWithTag("Enemy") == null || !wavesWaitForPreviousWaves)) {
				enemiesSpawned = 0;
				lastSpawnTime = Time.time;
			}
			// 5 
		} else {
			GameObject gameOverText = GameObject.FindGameObjectWithTag ("GameWon");
			gameOverText.GetComponent<Animator>().SetBool("gameOver", true);
		}
	}


	private void SpawnNext() {
		int currentWave = 0;
		lastSpawnTime = Time.time;
		GameObject newEnemy = (GameObject) Instantiate(waves[currentWave].enemyPrefab);
		newEnemy.GetComponent<MoveCar>().waypoints = waypoints;
		enemiesSpawned++;
	}

}

﻿using UnityEngine;
using System.Collections;

public class ToggleMenu : MonoBehaviour {

	public void Toggle() {
		gameObject.GetComponent<Animator> ().SetTrigger ("toggleMenu");
	}

}

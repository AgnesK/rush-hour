﻿using UnityEngine;
using System.Collections;

public class ToggleMode : MonoBehaviour {
	public enum Mode {Edit, Play};

	[SerializeField]
	private Mode currentMode;
	public Mode CurrentMode {
		get {
			return currentMode;
		}
	}

	// Use this for initialization
	public void Toggle () {
		// Set variable to next mode in enum, reset when reached last enum
		currentMode = (Mode)(((int) currentMode + 1) % System.Enum.GetNames(typeof(Mode)).Length);
		Camera.main.GetComponent<MoveCamera> ().ToggleCamera(currentMode);
	}

}
﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class WireView {
	public enum ConnectedDirection {Left, Right, Up, Down, Undefined};

	public ConnectedDirection direction1;
	public ConnectedDirection direction2;
	// Is this the connected or disconnected version, only makes difference when there are two wires in one cell.
	public bool connected;
	public Sprite sprite;
}

public class WireVisualizer : MonoBehaviour {
	public WireView[] wireViews;
	[SerializeField]
	private SpriteRenderer renderer1;
	[SerializeField]
	private SpriteRenderer renderer2;

	private SpriteRenderer lastDrawn;

	void Start() {
		lastDrawn = renderer2;
	}

	public void SetView(Vector2 entry, Vector2 exit) {
		WireView.ConnectedDirection direction1 = getWireViewDirection(entry);
		WireView.ConnectedDirection direction2 = getWireViewDirection(exit);

		foreach (WireView wireView in wireViews) {
			if ((wireView.direction1 == direction1 && wireView.direction2 == direction2) ||
			    (wireView.direction1 == direction2 && wireView.direction2 == direction1)) {
				if (lastDrawn == renderer1) {
					renderer2.sprite = wireView.sprite;
					lastDrawn = renderer2;
				} else {
					renderer1.sprite = wireView.sprite;
					lastDrawn = renderer1;
				}
			}
		}
	}

	private WireView.ConnectedDirection getWireViewDirection(Vector2 directionVector) {
		WireView.ConnectedDirection direction = WireView.ConnectedDirection.Undefined;
		if (directionVector.x == -1) {
			direction = WireView.ConnectedDirection.Left;
		} else if (directionVector.x == 1) {
			direction = WireView.ConnectedDirection.Right;
		} else if (directionVector.y == 1) {
			direction = WireView.ConnectedDirection.Down;
		} else if (directionVector.y == -1) {
			direction = WireView.ConnectedDirection.Up;
		}
		return direction;
	}

}